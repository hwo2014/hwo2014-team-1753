package elopod;

import com.google.gson.Gson;

abstract public class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
    
    public Double gameTick;
}