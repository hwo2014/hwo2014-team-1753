package elopod;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import elopod.in.carPosition.CarPosition;
import elopod.in.carPosition.CarPositions;
import elopod.in.carPosition.PiecePosition;
import elopod.in.gameInit.Car;
import elopod.in.gameInit.GameInit;
import elopod.in.gameInit.Lane;
import elopod.in.gameInit.Piece;
import elopod.in.gameInit.Track;
import elopod.in.lapFinished.LapFinished;
import elopod.in.lapFinished.LapTime;
import elopod.in.turbo.TurboAvailable;
import elopod.out.Ping;
import elopod.out.SwitchLane;
import elopod.out.SwitchLane.Dir;
import elopod.out.Throttle;

public class Racer {

	private String identifier;
	private String key;

	private PrintWriter writer;
	private BufferedReader reader;
	private Socket socket;
	private final Gson gson = new Gson();
	private GameInit config;
	private CarPositions carPositions;
	
	private boolean started;
	private String name;
	private Driver driver;
	private CarPosition myCarPosition;
	private Car myCar;
	
	int lastIndex;
	double istSpeed = 0;
	double lastDistance;
	private MapAnalyse mapAnalyse;
	
	private Track track;
	private boolean switching;
	double friction = 5;
	double accerlation;
	private boolean calcFraction;
	TurboAvailable info;
	public Integer currentLap;
	public boolean turboAvailable;
	private Double gameTick;
	
	double sendedSpeed;
	
	public Racer(String host, int port, String botName, String botKey) throws UnsupportedEncodingException, IOException {
		this.identifier = botName;
		this.name = botName;
		this.key = botKey;
		socket = new Socket(host, port);
		writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
		reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
		driver = new Driver(this);
	}

	public void stop() throws IOException {
		System.out.println("Round Ended!");
		socket.close();
	}

	public void tryHard() throws JsonSyntaxException, IOException {
		String line = null;
		while ((line = reader.readLine()) != null) {
			final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
			
			switch (msgFromServer.msgType) {
				case "carPositions":
					carPositions = gson.fromJson(line, CarPositions.class);
					resolveMyCarPosition();
					break;
				case "join":
					System.out.println("Joind");
					break;
				case "gameInit":
					config = gson.fromJson(line, GameInit.class);
					track = config.getData().getRace().getTrack();
					
					resolveMyCar();
					mapAnalyse = new MapAnalyse(this);
					processLanes();
					break;
				case "gameEnd":
					System.out.println("Game End");
					break;
				case "gameStart":
					System.out.println("Game Start");
					started = true;
					break;
					
				case "crash":
					driver.crash();
					sendedSpeed = 0;
					System.err.println("CRASH!");
					break;
	
//				case "yourCar":
//					Map data = (Map) msgFromServer.data;
//					name = (String) data.get("name");
//					System.out.println("Our Racer: " + data.get("name") + " [" + data.get("color") + "]");
//					break;
	
				case "lapFinished" : 
					LapFinished finished = gson.fromJson(line, LapFinished.class);
					String name = finished.getData().getCar().getName();
					LapTime laptime = finished.getData().getLapTime();
					if(name.equals(this.name)) {
						this.currentLap = laptime.getLap();
					}
					System.err.println("Runde " + laptime.getLap() + " beendet " + name + " mit " + laptime.getMillis() );
					break;
				case "turboAvailable" : 
					info = gson.fromJson(line, TurboAvailable.class);
					this.turboAvailable = true;
					break;
					
				default:
					System.out.println(line);
					send(new Ping());
			}
			
			System.out.println(line);
			
			if(msgFromServer.gameTick != null) {
				gameTick = msgFromServer.gameTick; 
			}
			
			if(started) {
				driver.drive();
			}
		}
	}

	private void processLanes() {
		List<Lane> lanes = track.getLanes();
		
		int left = 0, right = 0;
		
		for(Lane lane: lanes) {
			if(lane.getDistanceFromCenter() < left) {
				left = lane.getDistanceFromCenter();
			} else if(lane.getDistanceFromCenter() > right) {
				right = lane.getDistanceFromCenter();
			}
		}
		
		for(Lane lane: lanes) {
			if(lane.getDistanceFromCenter() == left) {
				lane.setLeft(true);
			} else if(lane.getDistanceFromCenter() == right) {
				lane.setRight(true);
			}
		}
	}

	private void resolveMyCar() {
		List<Car> cars = config.getData().getRace().getCars();
		for (Car car : cars) {
			if(car.getId().getName().equals(name)) {
				myCar = car;
				return;
			}
		}
	}
	
	private void resolveMyCarPosition() {
		myCarPosition = null;
		List<CarPosition> cars = carPositions.getData();
		for (CarPosition car : cars) {
			if(car.getId().getName().equals(getName())) {
				myCarPosition = car;
				break;
			}
		}
		
		PiecePosition piecePosition = myCarPosition.getPiecePosition();
		int pieceIndex = piecePosition.getPieceIndex();
		if(lastIndex == pieceIndex ) {
			istSpeed = piecePosition.getInPieceDistance() - lastDistance;
			
			if(!calcFraction && istSpeed != 0) {
				calcFraction = true;
				friction = accerlation / istSpeed;
				System.out.println("Friction: " + friction);
			}
		} else {
			lastIndex = pieceIndex;
			mapAnalyse.roundEnd();
		}
		lastDistance = piecePosition.getInPieceDistance();
		
	}

	public void speedUp(double speed) {
		accerlation = Math.min(speed, 1);
		
			send(new Throttle(accerlation));
	}

	/**
	 * @return the cars
	 */
	public CarPositions getCars() {
		return carPositions;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return the driver
	 */
	public Driver getDriver() {
		return driver;
	}
	
	/**
	 * @return the config
	 */
	public GameInit getConfig() {
		return config;
	}
	
	public void send(final SendMsg msg) {
		msg.gameTick = gameTick;
		send(msg.toJson());
	}
	
	public void send(final Object jsonObject) {
		send(gson.toJson(jsonObject));
	}
	
	private void send(final String msg) {
		writer.println(msg);
		writer.flush();
	}
	
	public Piece getCurrentPiece() {
		return track.getPieces().get(getMyCarPosition().getPiecePosition().getPieceIndex());
	}
	
	public MapAnalyse getMapAnalyse() {
		return mapAnalyse;
	}
	
	public Piece getRelativePiece(int relative) {
		PiecePosition piecePosition = getMyCarPosition().getPiecePosition();
		Integer pieceIndex = piecePosition.getPieceIndex();
		
		int wishedIndex = (pieceIndex + relative) % track.getPieces().size();
		if(wishedIndex < 0) {
			wishedIndex = track.getPieces().size() + wishedIndex;
		}
		Piece nextPiece = track.getPieces().get(wishedIndex);
		return nextPiece;
	}
	
	/**
	 * @return the myCar
	 */
	public CarPosition getMyCarPosition() {
		return myCarPosition;
	}
	
	/**
	 * @return the istSpeed
	 */
	public double getIstSpeed() {
		return istSpeed;
	}
	
	/**
	 * @return the myCar
	 */
	public Car getMyCar() {
		return myCar;
	}

	/**
	 * @return
	 */
	public boolean isStarted() {
		return started;
	}
	
	public boolean isOnLeftLane() {
		int laneIdx = getMyCarPosition().getPiecePosition().getLane().getStartLaneIndex();
		return track.getLanes().get(laneIdx).isLeft();
	}
	
	public boolean isOnRightLane() {
		int laneIdx = getMyCarPosition().getPiecePosition().getLane().getStartLaneIndex();
		return track.getLanes().get(laneIdx).isRight();
	}

	public void switchLane(Dir dir) {
		SwitchLane msg = new SwitchLane(dir);
		System.out.println("Sending: " + gson.toJson(msg));
		
		send(msg);
	}
	
	public void setSwitching(boolean switching) {
		this.switching = switching;
	}

	public boolean isSwitching() {
		return switching;
	}
	
	/**
	 * @return the track
	 */
	public Track getTrack() {
		return track;
	}
	
	/**
	 * @return the info
	 */
	public TurboAvailable getTurboInfo() {
		return info;
	}
	
	public void boost() {
		if(info != null) {
			SendMsg turbo = new SendMsg() {
				@Override
				protected String msgType() {
					return "turbo";
				}
				/* (non-Javadoc)
				 * @see elopod.SendMsg#msgData()
				 */
				@Override
				protected Object msgData() {
					return "Zzchneller Meizzter!";
				}
			};
			send(turbo);
			turboAvailable = false;
			System.out.println("Racer.boost()");
		}
	}
}
