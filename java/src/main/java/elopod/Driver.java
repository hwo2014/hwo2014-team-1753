/**
 * 
 */
package elopod;

import java.util.List;

import elopod.in.carPosition.CarPosition;
import elopod.in.carPosition.PiecePosition;
import elopod.in.gameInit.Piece;
import elopod.out.SwitchLane.Dir;
import elopod.util.MathUtils;

/**
 * @author Kani
 *
 */
public class Driver {

	/**
	 * @param racer
	 */
	public double velocity = 1;
	public double sollSpeed = 0;
	
	
	double k = 1;
	private Racer racer;
	
	int switchNum = 0;
	private boolean boosting;
	private int maxAngle = 40;
	
	/**
	 * 
	 */
	public Driver(Racer racer) {
		this.racer = racer;
	}
	
	public void drive() {
		if(!racer.isStarted()) {
			return;
		}
		
		if(switchHandling()) {
			
		} else if(agressive()) {
				
		} else {
//			Piece nextPiece = racer.getRelativePiece(1);
//			if (nextPiece.boost && !boosting && racer.turboAvailable) {
//				racer.boost();
//				boosting = true;
//			} else {
				PiecePosition piecePosition = racer.getMyCarPosition().getPiecePosition();
				
//				if(racer.lastIndex != racer.getMyCarPosition().getPiecePosition().getPieceIndex()) {
//					if(racer.getMyCarPosition().getAngle() < 20) {
//						racer.getMapAnalyse().getInformation(racer.lastIndex-1).speed *= 1.2;
//					}
//				}
				
				double mix = piecePosition.getInPieceDistance() / racer.getRelativePiece(0).getLength();
				
				sollSpeed = racer.getMapAnalyse().getSpeed(piecePosition.getPieceIndex(), mix);

				if(mix > 0.5) {
					double angleDif = racer.getMyCarPosition().getAngle() / maxAngle;            //|
					 
					angleDif -= 0.3;                                                       //|
					Piece piece = racer.getRelativePiece(-1);                              //|
					
					if(piece.isCurve()) {
						piece.modifySpeed(piece.getSpeed() - angleDif);                        //|
					}
				}
				
				toWarpSpeed();
//			}
//			
//			if (!nextPiece.boost) {
//				boosting = false;
//			}
		}
	}

	/**
	 * @return
	 */
	private boolean agressive() {
		List<CarPosition> data = racer.getCars().getData();
		
		Integer myIndex = racer.getMyCarPosition().getPiecePosition().getPieceIndex();
		
		if(racer.turboAvailable) {
			for (CarPosition carPosition : data) {
				Integer enemieIndex = carPosition.getPiecePosition().getPieceIndex();
				if(myIndex + 1 == enemieIndex && carPosition.getPiecePosition().getLane().getEndLaneIndex() == racer.getMyCarPosition().getPiecePosition().getLane().getEndLaneIndex()) {
					racer.boost();
					return true;
				}
			}
		}
		
		return false;
	}

	private boolean switchHandling() {
		Piece nextPiece = racer.getRelativePiece(1);
		boolean laneSwitched = false;
		if(nextPiece.getSwitch()) {
			if(!racer.isSwitching())  {
				laneSwitched = processLaneSwitch(nextPiece);
				racer.setSwitching(laneSwitched);
			}
		} else {
			racer.setSwitching(laneSwitched);
		}
		
		return laneSwitched;
	}

	private boolean processLaneSwitch(Piece piece) {
		double angle = 0;
		
		int relativePieceIndex = 2;
		do {
			Double pieceAngle = piece.getAngle();

			if(pieceAngle != null) {
				angle += pieceAngle;
			}
			
			piece = racer.getRelativePiece(relativePieceIndex++);
		} while (!piece.getSwitch());
		
		if(angle > 0) {
			if(!racer.isOnRightLane()) {
				racer.switchLane(Dir.RIGHT);
				return true;
			}
		} else if(angle < 0) {
			if(!racer.isOnLeftLane()) {
				racer.switchLane(Dir.LEFT);
				return true;
			}
		} 
		
		return false;
	}
	
	private void toWarpSpeed() {
		double difSpeed = sollSpeed - racer.getIstSpeed();
		System.out.println("soll: " + sollSpeed + " ist: " + racer.getIstSpeed() + " dif: " + difSpeed + " velo: " + velocity);
		k = 5 / racer.friction;
		velocity =+ difSpeed * k;
		velocity = MathUtils.clamp((float)velocity, 0, 1);
		racer.speedUp(velocity);
	}

	/**
	 * 
	 */
	void crash() {
		System.out.println("Crash Winkel: " + racer.getMyCarPosition().getAngle());
		Piece piece = racer.getRelativePiece(0);
		piece.modifySpeed(piece.getSpeed() * 0.6);
		piece = racer.getRelativePiece(-1);
		piece.modifySpeed(piece.getSpeed() * 0.6);
		piece = racer.getRelativePiece(-2);
		piece.modifySpeed(piece.getSpeed() * 0.6);
		piece = racer.getRelativePiece(-3);
		piece.modifySpeed(piece.getSpeed() * 0.6);
	}
}
