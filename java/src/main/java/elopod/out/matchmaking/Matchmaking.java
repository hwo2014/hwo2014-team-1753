
package elopod.out.matchmaking;

import javax.annotation.Generated;

import com.google.gson.annotations.Expose;

@Generated("org.jsonschema2pojo")
public class Matchmaking {

	public static final String JOIN_RACE = "joinRace";
	public static final String CREATE_RACE = "createRace";
	
    @Expose
    private String msgType;
    @Expose
    private Data data;

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

}
