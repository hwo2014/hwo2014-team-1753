/**
 * 
 */
package elopod.out;

import elopod.SendMsg;

/**
 * @author Kani
 *
 */
public class SwitchLane extends SendMsg {

	Dir dir;
	
	/**
	 * @param dir
	 */
	public SwitchLane(Dir dir) {
		super();
		this.dir = dir;
	}

	/* (non-Javadoc)
	 * @see elopod.SendMsg#msgType()
	 */
	@Override
	protected String msgType() {
		return "switchLane";
	}
	
	/* (non-Javadoc)
	 * @see elopod.SendMsg#msgData()
	 */
	@Override
	protected Object msgData() {
		return dir.getName();
	}
	
	public enum Dir {
		LEFT("Left"),
		RIGHT("Right");
		
		private final String name;

		Dir(String name) {
			this.name = name;
		}
		
		/**
		 * @return the name
		 */
		public String getName() {
			return name;
		}
	}
}
