package elopod.out;

import elopod.SendMsg;

public class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}