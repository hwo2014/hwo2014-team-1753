/**
 * 
 */
package elopod;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import elopod.in.gameInit.Piece;

/**
 * @author Kani
 *
 */
public class MapAnalyse {
	Racer racer;

	private double maxSpeed;

	private double radiusFaktor;
	
	Map<Integer, List<Piece>> streightLanes = new HashMap<>();
	int currentLine = 0;

	private List<Piece> pieces;
	
	private CurveManagement curveManager;
	
	/**
	 * @param racer
	 */
	public MapAnalyse(Racer racer) {
		super();
		this.racer = racer;
		this.maxSpeed = 20 * 5 / racer.friction;
		this.radiusFaktor = 500;
		analyse();
	}

	private void analyse() {
		pieces = racer.getConfig().getData().getRace().getTrack().getPieces();
		this.curveManager = new CurveManagement(pieces);
		for (int ct = 0; ct < pieces.size(); ++ct) {
			calculateSpeedInfo(pieces.get(ct), ct);
			streightLineInfo(pieces.get(ct));
		}
		
		for (int i = 0; i < pieces.size() - 2; i++) {
			Piece currentPiece = pieces.get(i);
			Piece nextPiece = pieces.get(i + 1);
			Piece nextNextPiece = pieces.get(i + 2);
			
			if(!nextPiece.isCurve() && nextNextPiece.isCurve() && !currentPiece.isCurve()) {
				currentPiece.setAnalyseSpeed((currentPiece.getSpeed() + nextPiece.getSpeed()) / 2.5);
			} else if(nextPiece.isCurve() && !currentPiece.isCurve()) {
				currentPiece.setAnalyseSpeed((currentPiece.getSpeed() + nextPiece.getSpeed()) / 3);
			} else if(!nextPiece.isCurve() && currentPiece.isCurve()) {
				currentPiece.setAnalyseSpeed((currentPiece.getSpeed() + nextPiece.getSpeed()) / 2.5);
			} else {
				currentPiece.setAnalyseSpeed((currentPiece.getSpeed() + nextPiece.getSpeed()) / 2);
			}
			System.out.println(currentPiece.getSpeed());
		}
		
		boostLines();
	}

	/**
	 * @param piece
	 */
	private void streightLineInfo(Piece piece) {
		if(!piece.isCurve()) {
			List<Piece> streightLane = getStreightLane(currentLine);
			streightLane.add(piece);
		} else {
			currentLine++;
		}
		
		// Wenns das letzte ist schaun obs erste eine Lienie ist
		if(pieces.indexOf(piece) == pieces.size() - 1 && !pieces.get(0).isCurve() && !piece.isCurve()) {
			List<Piece> currentLane = getStreightLane(currentLine);
			List<Piece> firstLane = getStreightLane(0);
			firstLane.addAll(0, currentLane);
			currentLane.clear();
		}
	}
	
	private void boostLines() {
//		Data data = racer.getTurboInfo().getData();
		Collection<List<Piece>> values = streightLanes.values();
		
		List<Piece> longestStreight = null;
		for (List<Piece> list : values) {
			if(longestStreight == null || longestStreight.size() < list.size()) {
				longestStreight = list;
			}
		}
		
		
		longestStreight.get(0).boost = true;
	}

	private void calculateSpeedInfo(Piece piece, int index) {
		if(piece.isCurve()) {
			
			double inCurve = (pieces.get(index+1).isCurve())? 1.2 : 1.0;
			double radiusPart = (piece.getRadius() / (inCurve * radiusFaktor));
			piece.setSpeed(maxSpeed * radiusPart);
		} else {
			piece.setSpeed(maxSpeed);
		}
	}
	
	public List<Piece> getStreightLane(int i) {
		List<Piece> list = streightLanes.get(i);
		if(list == null) {
			list = new ArrayList<>();
			streightLanes.put(i, list);
		}
		return list;
	}
	
	public double getSpeed(int pieceIndex, double distanceToNext) {
		List<Curve> curves = curveManager.getCurves();
		
		double percentage = 0;
		for (Curve curve : curves) {
			curve.setCurvePiecesLength(pieces);
			if(curve.getStartIndex() <= pieceIndex && curve.getEndIndex() >= pieceIndex) {
				double curveAngle = curve.getAngle();
				double angleInCurve = curveManager.getAngleInCurve(pieceIndex, 0);
				percentage = ((angleInCurve / curveAngle) > 0.99)? (angleInCurve / curveAngle) : 0;
			}
			
		}
		
		if(getInformation(pieceIndex).isCurve() && !getInformation(pieceIndex+1).isCurve())
			return getInformation(pieceIndex).getSpeed() * (5/ racer.friction);
		Piece first = getInformation(pieceIndex);
		Piece next = getInformation(pieceIndex + 1);

		double fromSpeed = (first.getSpeed() / (1 - percentage)) * (5 / racer.friction);
		double toSpeed = (next.getSpeed() / (1 - percentage)) * (5 / racer.friction);

		double d = (1d - distanceToNext) * fromSpeed + toSpeed * distanceToNext;
		return d < 1 ? 1 : d; 
	}
	
	public Piece getInformation(int pieceIndex) {
		return pieces.get(pieceIndex % pieces.size());
	}

	/**
	 * 
	 */
	public void roundEnd() {
		for (Piece piece : this.pieces) {
			piece.reset();
		}
	}
}
