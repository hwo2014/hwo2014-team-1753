package elopod;

import java.util.ArrayList;
import java.util.List;

import elopod.in.gameInit.Piece;

public class CurveManagement {
	private List<Piece> pieces;
	private List<Curve> curves;
	
	public CurveManagement(List<Piece> pieces) {
		super();
		this.pieces = pieces;
		
		this.findCurves();
	}

	private void findCurves() {
		List<Curve> curves = new ArrayList<>();
		Curve curveAtStart = null;
		
		int startIndex = 0;
		while(true) {
			startIndex = findNextCurveStart(startIndex);
			if(startIndex == -1) break;
			int endIndex = findCurveEnd(startIndex);
		
			Curve curve = new Curve(startIndex, endIndex);
			if(startIndex == 0)
				curveAtStart = curve;
			
			curves.add(curve);
			startIndex = endIndex+1;
		}
		
		if(curveAtStart != null && curves.get(curves.size()-1).getEndIndex() == curves.size()-1) {
			int startDirection = (curveAtStart.getAngle() >1)? 1 : -1;
			int endDirection = (curves.get(curves.size()-1).getAngle() >1)? 1 : -1;
			
			if(startDirection == endDirection) {
				curveAtStart.setStartIndex(curves.get(curves.size()-1).getStartIndex());
				curves.remove(curves.get(curves.size()-1));
			}
		}
		
		this.curves = curves;
	}

	public int findNextCurveStart(int ctStart) {
		for(int ct = ctStart; ct < this.pieces.size(); ++ct) {
			if(this.pieces.get(ct).getAngle() != null) {
				return ct;
			}
		}
		
		return -1;
	}
	
	public int findCurveEnd(int ctStart) {
		for(int ct = ctStart+1; ct < this.pieces.size(); ++ct) {
			int curveDirection = (this.pieces.get(ctStart).getAngle() >1)? 1 : -1;
			if(this.pieces.get(ct).getAngle() != null) {
				int pieceDirection = (this.pieces.get(ct).getAngle() >1)? 1 : -1;
			
				if(curveDirection == pieceDirection) {
					continue;
				}
			}
			
			return ct-1;
		}
		
		return this.pieces.size()-1;
	}
	
	public Double getAngleInCurve(int pieceIndex, double distance) {
		double angle = 0;
		for (Curve curve : this.curves) {
			if(curve.getStartIndex() <= pieceIndex && curve.getEndIndex() >= pieceIndex) {
				for(int ct = curve.getStartIndex(); ct < pieceIndex; ++ct) {
					angle += this.pieces.get(ct).getAngle();
				}
				return angle;
			}
		}
		return angle;
	}

	public List<Piece> getPieces() {
		return pieces;
	}

	public void setPieces(List<Piece> pieces) {
		this.pieces = pieces;
	}

	public List<Curve> getCurves() {
		return curves;
	}

	public void setCurves(List<Curve> curves) {
		this.curves = curves;
	}
}
