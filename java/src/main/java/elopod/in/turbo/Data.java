
package elopod.in.turbo;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("org.jsonschema2pojo")
public class Data {

    @Expose
    private Double turboDurationMilliseconds;
    @Expose
    private Integer turboDurationTicks;
    @Expose
    private Double turboFactor;

    public Double getTurboDurationMilliseconds() {
        return turboDurationMilliseconds;
    }

    public void setTurboDurationMilliseconds(Double turboDurationMilliseconds) {
        this.turboDurationMilliseconds = turboDurationMilliseconds;
    }

    public Integer getTurboDurationTicks() {
        return turboDurationTicks;
    }

    public void setTurboDurationTicks(Integer turboDurationTicks) {
        this.turboDurationTicks = turboDurationTicks;
    }

    public Double getTurboFactor() {
        return turboFactor;
    }

    public void setTurboFactor(Double turboFactor) {
        this.turboFactor = turboFactor;
    }

}
