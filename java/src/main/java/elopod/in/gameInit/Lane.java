
package elopod.in.gameInit;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("org.jsonschema2pojo")
public class Lane {

    @Expose
    private Integer distanceFromCenter;
    @Expose
    private Integer index;
	private boolean left;
	private boolean right;

    public Integer getDistanceFromCenter() {
        return distanceFromCenter;
    }

    public void setDistanceFromCenter(Integer distanceFromCenter) {
        this.distanceFromCenter = distanceFromCenter;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

	public void setLeft(boolean left) {
		this.left = left;
	}
	public boolean isLeft() {
		return left;
	}

	public void setRight(boolean right) {
		this.right = right;
	}
	public boolean isRight() {
		return right;
	}

}
