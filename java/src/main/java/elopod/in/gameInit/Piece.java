
package elopod.in.gameInit;

import javax.annotation.Generated;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import elopod.util.Vector2;


@Generated("org.jsonschema2pojo")
public class Piece {

    @Expose
    private Double length;
    @SerializedName("switch")
    @Expose
    private Boolean _switch;
    @Expose
    private Integer radius;
    @Expose
    private Double angle;

    public Vector2 debugStart;
    public Vector2 debugEnd;
	public boolean boost;
	private double speed;
	public double analyseSpeed;
    
	public boolean modified = false;
	
    public Double getLength() {
    	if(length == null && angle != null) {
    		length = radius * Math.PI * 2 * (360 / angle); 
    	}
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public Boolean getSwitch() {
        return _switch != null && _switch;
    }

    public void setSwitch(Boolean _switch) {
        this._switch = _switch;
    }

    public Integer getRadius() {
        return radius;
    }

    public void setRadius(Integer radius) {
        this.radius = radius;
    }

    public Double getAngle() {
        return angle;
    }

    public void setAngle(Double angle) {
        this.angle = angle;
    }

    public boolean isCurve() {
    	 return angle != null;
    }

	/**
	 * @return the speed
	 */
	public double getSpeed() {
		return speed;
	}

	/**
	 * @param speed the speed to set
	 */
	public void setSpeed(double speed) {
		this.speed = speed;
	}
	
	public void modifySpeed(double speed) {
		if(!modified) {
			if(speed < 1) {
				speed = 1;
			}
			this.speed = speed;
			modified = true;
		}
	}
	
	public void reset() {
		modified = false;
	}

	/**
	 * @param d
	 */
	public void setAnalyseSpeed(double d) {
		speed = d;
		analyseSpeed = d;
	}
}
