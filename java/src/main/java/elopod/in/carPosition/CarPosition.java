
package elopod.in.carPosition;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("org.jsonschema2pojo")
public class CarPosition {

    @Expose
    private Id id;
    @Expose
    private Double angle;
    @Expose
    private PiecePosition piecePosition;

    public Id getId() {
        return id;
    }

    public void setId(Id id) {
        this.id = id;
    }

    public Double getAngle() {
        return angle == null ? 0 : angle;
    }

    public void setAngle(Double angle) {
        this.angle = angle;
    }

    public PiecePosition getPiecePosition() {
        return piecePosition;
    }

    public void setPiecePosition(PiecePosition piecePosition) {
        this.piecePosition = piecePosition;
    }

}
