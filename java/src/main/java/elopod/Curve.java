package elopod;

import java.util.List;

import elopod.in.gameInit.Piece;

public class Curve {
	private double length = 0;
	private double angle = 0;
	private double radius = 0;
		
	private int startIndex;
	private int endIndex;
	
	public Curve(int startIndex, int endIndex) {
		super();
		this.startIndex = startIndex;
		this.endIndex = endIndex;
	}
	
	public void setCurvePiecesLength(List<Piece> pieces) {
		if(this.startIndex <= this.endIndex) {
			for(int ct = this.startIndex; ct <= this.endIndex; ++ct) {
				this.calculateCurveLength(pieces.get(ct));
			}
		} else {
			for(int ct = this.startIndex; ct < pieces.size(); ++ct) {
				this.calculateCurveLength(pieces.get(ct));
			}
			for(int ct = 0; ct <= this.endIndex; ++ct) {
				this.calculateCurveLength(pieces.get(ct));
			}
		}
	}
	
	public Double calculateCurveLength(Piece curvePiece) {
		double angle = curvePiece.getAngle();
		double radius = curvePiece.getRadius();
		
		this.radius = (this.radius+radius)/2;
		this.angle += angle;
		
		double length = Math.PI*radius*(angle/180);
		
		curvePiece.setLength(length);
		this.length += length;
		
		return length;
	}



	public Double getLength() {
		return length;
	}



	public void setLength(Double length) {
		this.length = length;
	}



	public Double getAngle() {
		return angle;
	}



	public void setAngle(Double angle) {
		this.angle = angle;
	}



	public Double getRadius() {
		return radius;
	}



	public void setRadius(Double radius) {
		this.radius = radius;
	}



	public int getStartIndex() {
		return startIndex;
	}



	public void setStartIndex(int startIndex) {
		this.startIndex = startIndex;
	}



	public int getEndIndex() {
		return endIndex;
	}



	public void setEndIndex(int endIndex) {
		this.endIndex = endIndex;
	}
}
