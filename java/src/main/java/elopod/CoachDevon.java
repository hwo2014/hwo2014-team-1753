package elopod;

import java.util.ArrayList;
import java.util.List;

import elopod.in.carPosition.CarPosition;
import elopod.in.carPosition.PiecePosition;
import elopod.in.gameInit.Piece;
import elopod.util.MathUtils;

public class CoachDevon {
	public CoachDevon(Racer racer) {
		super();
		this.racer = racer;
		this.pieces = racer.getConfig().getData().getRace().getTrack().getPieces();
	}

	private Racer racer;
	private List<Piece> pieces;
	
	public int getNextCurve() {
		Integer pieceIndex = this.racer.getMyCarPosition().getPiecePosition().getPieceIndex();
		Double inPieceDistance = this.racer.getMyCarPosition().getPiecePosition().getInPieceDistance();
		List<Piece> pieces = this.racer.getConfig().getData().getRace().getTrack().getPieces();
		
		//index of next curve piece
		int curvePiece = -1;
		for(int ct = 0; ct <pieces.size(); ++ct) {
			if(pieces.get(ct).getAngle() != null && pieces.get(ct).getAngle() != 0 && ct > pieceIndex) {
				curvePiece = ct;
				break;
			}
			else if(curvePiece == -1 &&  pieces.get(ct).getAngle() != null && pieces.get(ct).getAngle() != 0) {
				curvePiece = ct;
				if(ct > pieceIndex) break;
			}
		}
		return curvePiece;
	}
	
	public double getDistanceToNextCurve(int nextCurve) {
		Integer pieceIndex = this.racer.getMyCarPosition().getPiecePosition().getPieceIndex();
		Double inPieceDistance = this.racer.getMyCarPosition().getPiecePosition().getInPieceDistance();
		List<Piece> pieces = this.racer.getConfig().getData().getRace().getTrack().getPieces();
		
		//distance between flag and next curve piece
		double distance = (pieces.get(pieceIndex).getLength() != null && inPieceDistance != null)?pieces.get(pieceIndex).getLength()-inPieceDistance : 0;
		
		if(nextCurve > pieceIndex) {
			for(int ct = pieceIndex+1; ct < nextCurve; ++ct) {
				distance += pieces.get(ct).getLength();
			}
		} else {
			for(int ct = pieceIndex+1; ct < pieces.size(); ++ct) {
				distance += pieces.get(ct).getLength();
			}
			for(int ct = 0; ct < nextCurve; ++ct) {
				distance += pieces.get(ct).getLength();
			}
		}
		
		return distance;
	}
	
	public Double newSpeed(double distance, int nextCurve) {
		Integer pieceIndex = this.racer.getMyCarPosition().getPiecePosition().getPieceIndex();
		Double inPieceDistance = this.racer.getMyCarPosition().getPiecePosition().getInPieceDistance();
		List<Piece> pieces = this.racer.getConfig().getData().getRace().getTrack().getPieces();
		
		
		double breakpoint = (distance*0.9)/this.racer.lastDistance;
		double sollSpeed = 0;
		if(breakpoint < 2.6 ) {
			sollSpeed = 8.5;
			
			
		} else {
			sollSpeed = 30;
		}
		
		
		Double rest = (pieces.get(pieceIndex).getLength() != null && inPieceDistance != null)?pieces.get(pieceIndex).getLength()-inPieceDistance : 100;
		Double ratio = (pieces.get(pieceIndex).getLength() != null && rest != null)? rest/pieces.get(pieceIndex).getLength() : 1;
		
		if(Math.abs(this.racer.getMyCarPosition().getAngle()) > 30)
			sollSpeed = 5;
			
		return sollSpeed;
	}
	
	public Double calculateCurveLength(int startIndex, int lane) {
		while(true) {
			
			
			break;
		}
		
		return null;
	}

	public Racer getRacer() {
		return this.racer;
	}

	public List<Curve> findCurves() {
		List<Curve> curves = new ArrayList<>();
		Curve curveAtStart = null;
		
		int startIndex = 0;
		while(true) {
			startIndex = findNextCurveStart(startIndex);
			if(startIndex == -1) break;
			int endIndex = findCurveEnd(startIndex);
		
			Curve curve = new Curve(startIndex, endIndex);
			if(startIndex == 0)
				curveAtStart = curve;
			
			curves.add(curve);
		}
		
		if(curveAtStart != null && curves.get(curves.size()-1).getEndIndex() == curves.size()-1) {
			int startDirection = (curveAtStart.getAngle() >1)? 1 : -1;
			int endDirection = (curves.get(curves.size()-1).getAngle() >1)? 1 : -1;
			
			if(startDirection == endDirection) {
				curveAtStart.setStartIndex(curves.get(curves.size()-1).getStartIndex());
				curves.remove(curves.get(curves.size()-1));
			}
		}
		
		return curves;
	}

	public int findNextCurveStart(int ctStart) {
		for(int ct = ctStart; ct < this.pieces.size(); ++ct) {
			if(this.pieces.get(ct).getAngle() != null) {
				return ct;
			}
		}
		
		return -1;
	}
	
	public int findCurveEnd(int ctStart) {
		for(int ct = ctStart+1; ct < this.pieces.size(); ++ct) {
			int curveDirection = (this.pieces.get(ctStart).getAngle() >1)? 1 : -1;
			if(this.pieces.get(ct).getAngle() != null) {
				int pieceDirection = (this.pieces.get(ct).getAngle() >1)? 1 : -1;
			
				if(curveDirection == pieceDirection) {
					continue;
				}
			}
			
			return ct-1;
		}
		
		return this.pieces.size()-1;
	}
	
	public void setRacer(Racer racer) {
		this.racer = racer;
	}
}
